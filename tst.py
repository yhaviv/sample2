import mlrun

def handler(context: mlrun.MLClientCtx, data: mlrun.DataItem):
    print(f"Run: {context.name} (uid={context.uid})")
    print(f"data of {data.url}:")
    print(data.get())
