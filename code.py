import mlrun

def handler(context: mlrun.MLClientCtx, p1=1, p2=3, p3=4):
    print(f"Run: {context.name} (uid={context.uid})")
    print("my line")
    context.log_result("accuracy", p2*2)
    context.log_result("loss", p3*3)
    context.log_artifact("file_result", body=b"abc123", local_path="result.txt")


